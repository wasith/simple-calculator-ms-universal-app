﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Simple_Calculator
{
    class Calculator
    {
        Stack<double> numberStack = new Stack<double>();
        Stack<string> operandStack = new Stack<string>();

        // An equation
        string equation = "";

        // Carries
        double x = 0, y = 0;
        double result = 0;

        public double calculate(string s)
        {
            this.equation = s.Trim();
            return calculate();
        }

        /*
         * Return first operand index
         * eg. getFirstOperandIndex("3.14+11.0");
         * return 4;
         * rg. getFirstOperandIndex("3.1415926");
         * return -1;
         */
        public int getFirstOperandIndex(string s)
        {
            int[] index = new int[4];
            index[0] = s.IndexOf('+');
            index[1] = s.IndexOf('-');
            index[2] = s.IndexOf('x');
            index[3] = s.IndexOf('/');

            int leastIndex = int.MaxValue;
            for (int i = 0; i < index.Length; ++i)
            {
                // -1 mean not found
                if (index[i] != -1)
                {
                    if (index[i] < leastIndex)
                    {
                        leastIndex = index[i];
                    }
                }
            }

            // if leastIndex == int.MaxValue meand not found any operand
            return leastIndex == int.MaxValue ? -1 : leastIndex;
        }

        private void subCal()
        {
            int operandIndex = getFirstOperandIndex(equation);
            string valueString = equation.Substring(0, operandIndex);
            equation = equation.Substring(operandIndex, equation.Length - operandIndex).Trim();
            y = Double.Parse(valueString);
            x = numberStack.Pop();
            operandStack.Pop();

            operandIndex = getFirstOperandIndex(equation);
            if (operandIndex == 0)
            {
                // Push Operand to operandStack
                string temp = equation.Substring(operandIndex, 1);
                operandStack.Push(temp);
                equation = equation.Substring(1, equation.Length - 1).Trim();
            }
        }

        private double calculate()
        {
            // Find for all string line
            for (int i = 0; i < equation.Length; ++i)
            {
                int operandIndex = getFirstOperandIndex(equation);
                if (operandIndex == -1)
                {
                    if (equation.Length == 0) { break; }

                    // Push number to numberStack
                    double value = Double.Parse(equation);
                    numberStack.Push(value);

                    // Clear all string in equation then break; loop
                    equation = "";
                    break;
                }

                // Push number to numberStack
                string number = equation.Substring(0, operandIndex);
                double d = Double.Parse(number);
                numberStack.Push(d);

                // Push operand to operandStack
                string operand = equation.Substring(operandIndex, 1);
                operandStack.Push(operand);

                // Cut string line
                equation = equation.Substring(++operandIndex, equation.Length - operandIndex).Trim();

                // if * or / then calculate
                if (operand == "*") { subCal(); numberStack.Push(x * y); }
                else if (operand == "/") { subCal(); numberStack.Push(x / y); }
                calculate();
            }

            // If still remeaing of any + - operand then compute it all
            while (operandStack.Count != 0)
            {
                if (numberStack.Count == 1) { break; }
                x = numberStack.Pop();
                y = numberStack.Pop();

                string c = operandStack.Pop();

                // if it + then add, if it - then subtract
                if (c == "+") { numberStack.Push(y + x); }
                else if (c == "-") { numberStack.Push(y - x); }
            }

            // if numberStack.Count == 1 then return it as result
            if (numberStack.Count == 1) { result = numberStack.Pop(); }
            return result;
        }
    }
}
